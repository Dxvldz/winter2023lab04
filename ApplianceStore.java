import java.util.Scanner;
public class ApplianceStore {
    public static void main(String[]args) {
        // Create new array of 4 appliances
        AirConditioner[] airConditioners = new AirConditioner[4];
        Scanner scan = new Scanner(System.in);

        for(int i = 0; i < airConditioners.length; i++){
            System.out.println("Input infomation for the Air conditioner number " + (i+1));

            // Ask user input
            System.out.println("What brand is the Air conditioner?");
            String brand = scan.next();
            // airConditioners[i].setBrand();

            System.out.println("What's the width of the Air conditioner?(cm)");
            double width = scan.nextDouble();
            // airConditioners[i].setWidth(scan.nextDouble());

            System.out.println("What's the depth of the Air conditioner?(cm)");
            double depth = scan.nextDouble();
            // airConditioners[i].setDepth(scan.nextDouble());

            System.out.println("What's the height of the Air conditioner?(cm)");
            double height = scan.nextDouble();
            // airConditioners[i].setHeight(scan.nextDouble());

            System.out.println("What's the temperature of the Room?");
            double temperature = scan.nextDouble();
            // airConditioners[i].setTemperature(scan.nextDouble());

            airConditioners[i] = new AirConditioner(brand,width,depth,height,temperature);
        }   


        // Printing last appliance informations
        airConditioners[3].informations();

        // call both instance methods for first appliance
        System.out.println("This is the cost of the first air conditioner : " + airConditioners[0].applianceCost());
        airConditioners[0].informations();

        System.out.println("The room was originally : " + airConditioners[1].getTemperature()  + " degrees.");
        airConditioners[1].temperatureChange(12);
        if(airConditioners[1].getRun()){
            System.out.println("The Air Conditionner is now running!") ;
        }
        System.out.println("The room is now : " + airConditioners[1].getTemperature() + " degrees.") ;

        System.out.println("Before");
        airConditioners[3].informations();
        
        airConditioners[3].setBrand("Carrier");
        airConditioners[3].setDepth(45);
        airConditioners[3].setHeight(34);
        airConditioners[3].setTemperature(3);
        airConditioners[3].setWidth(30);

        System.out.println("After");
        airConditioners[3].informations();
    }
}
