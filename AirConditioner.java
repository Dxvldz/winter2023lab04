public class AirConditioner {
    // All four fields
    private String brand;
    private double width;
    private double depth;
    private double height;
    private double temperature;
    private boolean run;

    /**
     * airCon temp 
     * Wished Temp
     * run
     * if(airCon > wish Temp){
     * run = off
     * }else {
     * run = on
     * }
     */
    

    public AirConditioner(String brand, double width, double depth, double height, double temperature) {
        this.brand = brand;
        this.width = width;
        this.depth = depth;
        this.height = height;
        this.temperature = temperature;
        this.run = false;
    }


    public void temperatureChange(double wantedTempeture) {
        if (this.isWantedTempeture(wantedTempeture)) {
            this.run = true;
            this.temperature = wantedTempeture ;
        }
        else {
            this.run = false;
        }    
    }

    private boolean isWantedTempeture (double wantedTempeture) {
        return this.temperature != wantedTempeture;
    }

    /* Cost of Air conditioner :
     * Carrier : 200$
     * Goodman : 180$
     * Trane: 160$
     * Lennox: 140$
     * Rhem: 120$
     * Others: 100$
     * Each cm3 = 0.004$
    */
    
    //This method calculate the dimension of the appliance
    public static double dimension (double width, double height, double depth) {
        return (width*height*depth);
    }

    // This method calculate the cost of the air conditioner with his dimension
    public double applianceCost () {
        double size = dimension(width,height,depth);
        double startingPrice;
        double cost;
                switch (brand.toLowerCase()) {
                    case "carrier" :
                        startingPrice = 200;
                        break;
                    case "goodman" :
                        startingPrice = 180;
                        break;
                    case "trane" :
                        startingPrice = 160;
                        break;
                    case "lennox" :
                        startingPrice = 140;
                        break;
                    case "rhem" : 
                        startingPrice = 120;
                        break;
                    default :
                        startingPrice = 100;
                }
        cost = startingPrice + (0.004*size);
        return cost;
    }

    //This method gives all informations about the product in a list
    public void informations () {
        // Display string, width, depth, height, total dimension and cost
        double totalDimension = dimension(width,height,depth);
        double cost = applianceCost();

        System.out.printf("Here is all the information about the air conditioner : \nBrand : %s \nWidth : %.2f cm\nDepth : %.2f cm\nHeight : %.2f cm\nTotal dimension : %.2f cm3\nCost : %.2f$\nTemperature : %.2f degrees Celsius\n", brand, width, depth, height, totalDimension, cost,temperature);
    }


    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getWidth() {
        return this.width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getDepth() {
        return this.depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }

    public double getHeight() {
        return this.height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getTemperature() {
        return this.temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public boolean isRun() {
        return this.run;
    }

    public boolean getRun() {
        return this.run;
    }

    public void setRun(boolean run) {
        this.run = run;
    }
    
}

